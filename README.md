# Sonktionary

Sonktionary est une table de son (aussi appelée « <i lang="en">sound board</i> ») avec des mots issus du [Wiktionnaire](https://fr.m.wiktionary.org).
Le principe est simple : vous cliquez sur un mot, Sonktionary prononce le mot.

## Utilisation

Rendez-vous sur : [marienfressinaud.frama.io/sonktionary](https://marienfressinaud.frama.io/sonktionary/)

## Prototype

Sonktionary est un prototype.
Il fonctionne, mais il pourrait être être amélioré.
Je ne compte toutefois pas continuer à bosser dessus.

En plus le code est dégueu.

## Licence

Sonktionary est disponible sous [WTFPL](/LICENSE.txt).
